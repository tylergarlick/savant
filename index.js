"use strict";

var Traceur = require('traceur');
Traceur.require.makeDefault(function (filename) {
  return filename.indexOf('node_modules') === -1;
});

module.exports = require('./lib');