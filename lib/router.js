"use strict";

export var endpoints = [
  { method: 'GET', path: '/{param*}',
    handler: {
      directory: {
        path: 'public'
      }
    }
  }
];
