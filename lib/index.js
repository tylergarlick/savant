"use strict";

var Hapi = require('hapi');
var server = new Hapi.Server(3000);

import {endpoints} from './router.js';

server.route(endpoints);


server.start(function () {
  console.log('Server running at:', server.info.uri);
});

//server.pack.register([
//  {
//    plugin: require('boom')
//  },
//  {
//    plugin: require('poop')
//  },
//  {
//    plugin: require('scooter')
//  }
//], function (err) {
//
//});